FROM ubuntu:20.04

# Installing as root: docker images are usually set up as root.
# Since some autotools scripts might complain about this being unsafe, we set
# FORCE_UNSAFE_CONFIGURE=1 to avoid configure errors.
ENV FORCE_UNSAFE_CONFIGURE=1
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update
RUN apt-get install -y \
    sudo \
    git	 \
    autoconf \
    automake \
    build-essential \
    libtool-bin \
    libhwloc-dev \
    pkg-config \
    gdb \
    vim \
    openmpi-bin \
    openmpi-common \
    nvidia-cuda-toolkit \
    libsimgrid-dev \
    libfxt-dev \
    libopenblas-pthread-dev

# create script to test starpu
RUN mkdir /home/starpu
COPY test_starpu.sh /home/starpu
COPY bashrc_starpu.sh /home/starpu/.bashrc
RUN chmod +x /home/starpu/test_starpu.sh

# change the default shell to be bash
SHELL ["/bin/bash", "-c"]

# Create the starpu user
RUN useradd -m starpu
RUN echo 'starpu ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers
ENV SHELL=/bin/bash
RUN chown -R starpu:starpu /home/starpu
USER starpu
WORKDIR /home/starpu


