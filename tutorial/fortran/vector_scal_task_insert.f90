! StarPU --- Runtime system for heterogeneous multicore architectures.
!
! Copyright (C) 2022-2024   University of Bordeaux, CNRS (LaBRI UMR 5800), Inria
!
! StarPU is free software; you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation; either version 2.1 of the License, or (at
! your option) any later version.
!
! StarPU is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
!
! See the GNU Lesser General Public License in COPYING.LGPL for more details.
!
! This example demonstrates how to use StarPU to scale an array by a factor.
! It shows how to manipulate data with StarPU's data management library.
!  1- how to declare a piece of data to StarPU (fstarpu_vector_data_register)
!  2- how to submit a task to StarPU (fstarpu_task_insert)
!  3- how a kernel can manipulate the data (buffers[0].vector.ptr)
program vector_scal
        use iso_c_binding       ! C interfacing module
        use fstarpu_mod         ! StarPU interfacing module
        use vector_scal_cl
        use vector_scal_cl_cuda
        implicit none

        real, dimension(:), allocatable, target :: vector
        integer :: i, ret_pin, ret_unpin
        integer(c_int) :: NX=2048 !NX=1024~2097152
        real, target :: factor = 3.14
        integer(c_int), target :: r = 1

        type(c_ptr) :: perfmodel_vec   ! a pointer for the perfmodel structure
        type(c_ptr) :: scal_cl   ! a pointer for the codelet structure
        type(c_ptr) :: vector_handle    ! a pointer for the 'vector' vector data handle
        integer(c_int) :: err   ! return status for fstarpu_init
        integer(c_int) :: ncpu  ! number of cpus workers

        real(c_double) :: start_time ! start clock in usec
        real(c_double) :: end_time   ! end clock in usec

        integer :: start_count, end_count, count_rate, count_max

        ! initialize StarPU
        err = fstarpu_init(C_NULL_PTR)

        ! allocate vector
        allocate(vector(NX))
        ! Register for CUDA asynchronous transfers
        ! Will be available in starpu-1.4
        ! ret_pin = fstarpu_memory_pin(c_loc(vector), INT8(NX)*c_sizeof(vector(0)))
        vector = REAL(1)

        write(*,*) "BEFORE: First element was", vector(1)

        ! allocate an empty perfmodel structure
        perfmodel_vec = fstarpu_perfmodel_allocate()

        ! set the perfmodel symbol
        call fstarpu_perfmodel_set_symbol(perfmodel_vec, C_CHAR_"vector_scal_perf"//C_NULL_CHAR)

        ! set the perfmodel type
        call fstarpu_perfmodel_set_type(perfmodel_vec, FSTARPU_NL_REGRESSION_BASED)

        ! allocate an empty codelet structure
        scal_cl = fstarpu_codelet_allocate()

        ! set the codelet perfmodel
        call fstarpu_codelet_set_model(scal_cl, perfmodel_vec)

        ! set the codelet name
        call fstarpu_codelet_set_name(scal_cl, C_CHAR_"vector_scal_codelet"//C_NULL_CHAR)

        ! add a CPU implementation function to the codelet
        call fstarpu_codelet_add_cpu_func(scal_cl, C_FUNLOC(vector_scal_cpu))

#if defined (STARPU_SIMGRID)
        !CUDA pseudo-implementation of the codelet
        call fstarpu_codelet_add_cuda_func(scal_cl, C_FUNLOC(c_loc(r)))
#elif defined (STARPU_USE_CUDA)
        ! add a CUDA implementation function to the codelet
        call fstarpu_codelet_add_cuda_func(scal_cl, C_FUNLOC(vector_scal_cuda_cl))
#endif

        ! add a Read-Write mode data buffer to the codelet
        call fstarpu_codelet_add_buffer(scal_cl, FSTARPU_RW)

        ! register 'vector'
        call fstarpu_vector_data_register(vector_handle, 0, c_loc(vector), NX, c_sizeof(vector(0)))

        ! collect the start clock time
        ! Will be available in starpu-1.4
        ! start_time = fstarpu_timing_now()
        call system_clock(start_count, count_rate, count_max)

        ! submit a task with codelet scal_cl, and vector 'vector'
        !
        call fstarpu_task_insert((/ scal_cl, &
                FSTARPU_RW, vector_handle, &
                FSTARPU_VALUE, c_loc(factor), FSTARPU_SZ_C_FLOAT, &
                C_NULL_PTR /))

        ! wait for task completion
        call fstarpu_task_wait_for_all()

        ! Will be available in starpu-1.4
        ! end_time = fstarpu_timing_now()
        call system_clock(end_count, count_rate, count_max)

        ! unregister 'vector'
        call fstarpu_data_unregister(vector_handle)

        write(*,*) "AFTER: First element is", vector(1)

        ! Unregister for CUDA asynchronous transfers
        ! Will be available in starpu-1.4
        ! ret_unpin = fstarpu_memory_unpin(c_loc(vector), INT8(NX)*c_sizeof(vector(0)))

        deallocate(vector)

        ! free codelet structure
        call fstarpu_codelet_free(scal_cl)

        ! shut StarPU down
        call fstarpu_shutdown()

        ! free perfmodel structure (must be called after fstarpu_shutdown)
        call fstarpu_perfmodel_free(perfmodel_vec)

        ! print '(a, es 10.3, a)', 'computation took:', end_time - start_time, ' µs.'
        print '(a, es 10.3, a)', 'computation took:', real(start_count - end_count) / count_rate * 1000000, ' µs.'

end program vector_scal
