! StarPU --- Runtime system for heterogeneous multicore architectures.
!
! Copyright (C) 2022-2024   University of Bordeaux, CNRS (LaBRI UMR 5800), Inria
!
! StarPU is free software; you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation; either version 2.1 of the License, or (at
! your option) any later version.
!
! StarPU is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
!
! See the GNU Lesser General Public License in COPYING.LGPL for more details.
!

module insert_task_cl
contains
recursive subroutine cl_cpu_func (buffers, cl_args) bind(C)
        use iso_c_binding       ! C interfacing module
        use fstarpu_mod         ! StarPU interfacing module
        implicit none

        type(c_ptr), value, intent(in) :: buffers, cl_args ! cl_args is unused
        integer(c_int), pointer :: x, y

        call c_f_pointer(fstarpu_variable_get_ptr(buffers, 0), x)
        call c_f_pointer(fstarpu_variable_get_ptr(buffers, 1), y)

        write(*,*) "VALUES: ", x, y
        x = (x + y) / 2

end subroutine cl_cpu_func

function my_distrib(x, y, size) bind(C)
        use iso_c_binding       ! C interfacing module

        integer(c_int), intent(in) :: x, y, size
        integer(c_int) :: my_distrib

        my_distrib = mod((x + y), size)

end function

end module insert_task_cl

program insert_task
        use iso_c_binding       ! C interfacing module
        use fstarpu_mod         ! StarPU interfacing module
        use fstarpu_mpi_mod     ! StarPU MPI interfacing module
        use insert_task_cl
        implicit none


        integer(c_int), allocatable, target :: matrix(:,:)
        integer(c_int) :: x,y
        integer(c_int) :: NX = 4
        integer(c_int) :: NY = 5
        integer(c_int) :: ret
        integer(c_int), target :: world
        integer(c_int) :: rank
        integer(c_int) :: size
        type(c_ptr), allocatable :: matrix_handle(:,:)
        type(c_ptr) :: insert_cl   ! a pointer for the codelet structure
        integer(c_int) :: mpi_rank
        integer(c_int) :: value = 0
        integer(c_int64_t) :: tag

        ret = fstarpu_init(C_NULL_PTR)
        ret = fstarpu_mpi_init(1)

        world = fstarpu_mpi_world_comm()
        rank = fstarpu_mpi_world_rank()
        size = fstarpu_mpi_world_size()

        !allocation
        allocate(matrix(NX,NY))
        allocate(matrix_handle(NX,NY))

        do x=1, NX
        do y=1, NY
                matrix(x,y) = (rank+1)*10 + value
                value = value + 1
        end do
        end do

        ! allocate an empty codelet structure
        insert_cl = fstarpu_codelet_allocate()

        ! set the codelet name
        call fstarpu_codelet_set_name(insert_cl, C_CHAR_"insert_codelet"//C_NULL_CHAR)

        ! add a CPU implementation function to the codelet
        call fstarpu_codelet_add_cpu_func(insert_cl, C_FUNLOC(cl_cpu_func))

        ! add a Read-Write mode data buffer to the codelet
        call fstarpu_codelet_add_buffer(insert_cl, FSTARPU_RW)
        ! add a Read mode data buffer to the codelet
        call fstarpu_codelet_add_buffer(insert_cl, FSTARPU_R)

        tag = 0
        do x=1, NX
        do y=1, NY
                mpi_rank = my_distrib(x-1, y-1, size)
                if(rank == mpi_rank) then
                        call fstarpu_variable_data_register(matrix_handle(x,y), 0, &
                         c_loc(matrix(x,y)), c_sizeof(matrix(1,1)))
                else
                        call fstarpu_variable_data_register(matrix_handle(x,y), -1, &
                        C_NULL_PTR, c_sizeof(matrix(1,1)))
                end if

                if(c_associated(matrix_handle(x,y))) then
                        call fstarpu_mpi_data_register(matrix_handle(x,y), tag, mpi_rank)
                        tag = tag+1
                end if
        end do
        end do

        call fstarpu_mpi_task_insert((/ c_loc(world), insert_cl, &
                   FSTARPU_RW,  matrix_handle(2,2), &
                   FSTARPU_R,  matrix_handle(1,2), &
                   C_NULL_PTR /))

        call fstarpu_mpi_task_insert((/ c_loc(world), insert_cl, &
                   FSTARPU_RW, matrix_handle(4,2), &
                   FSTARPU_R, matrix_handle(1,2), &
                   C_NULL_PTR /))
        call fstarpu_mpi_task_insert((/ c_loc(world), insert_cl, &
                   FSTARPU_RW, matrix_handle(1,2), &
                   FSTARPU_R, matrix_handle(1,1), &
                   C_NULL_PTR /))
        call fstarpu_mpi_task_insert((/ c_loc(world), insert_cl, &
                   FSTARPU_RW, matrix_handle(4,2), &
                   FSTARPU_R, matrix_handle(1,2), &
                   C_NULL_PTR /))

        print *, "Waiting..."

        ! wait for task completion
        call fstarpu_task_wait_for_all()

        ! unregister handle
        do x=1, NX
        do y=1, NY
                if(c_associated(matrix_handle(x,y))) then
                        call fstarpu_data_unregister(matrix_handle(x, y))
                end if
        end do
        end do


        ! free codelet structure
        call fstarpu_codelet_free(insert_cl)

        ! shut MPI down
        ret = fstarpu_mpi_shutdown()

        ! shut StarPU down
        call fstarpu_shutdown()

        deallocate(matrix)
        deallocate(matrix_handle)

end program insert_task
