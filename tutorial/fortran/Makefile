# StarPU --- Runtime system for heterogeneous multicore architectures.
#
# Copyright (C) 2022-2024  Université de Bordeaux, CNRS (LaBRI UMR 5800), Inria
#
# StarPU is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation; either version 2.1 of the License, or (at
# your option) any later version.
#
# StarPU is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#
# See the GNU Lesser General Public License in COPYING.LGPL for more details.
#

PROG = vector_scal0 vector_scal_task_insert vector_scal_task_insert_filter
OBJS = vector_scal_cl.o

include ./starpu.mk

vector_scal_task_insert: vector_scal_cl.o fstarpu_mod.o vector_scal_task_insert.o
vector_scal_task_insert_filter: vector_scal_cl.o fstarpu_mod.o vector_scal_task_insert_filter.o
vector_scal_cl.o: fstarpu_mod.o

ifeq ($(USE_CUDA),1)
FCFLAGS+=-DSTARPU_USE_CUDA=1
vector_scal_task_insert: vector_scal_cuda.o
vector_scal_task_insert_filter: vector_scal_cuda.o
$(PROG): LDLIBS+=-L$(CUDA_PATH)/lib64 -lcudart -lstdc++
endif
