
## Linux docker images

This directory contains docker recipes for several images.

- `registry.gitlab.inria.fr/starpu/starpu-docker/ci` is a minimal
     image to compile StarPU. The file recipe is
     [Dockerfile](./Dockerfile)

To use gdb within the docker image, one needs to

```bash
docker run --privileged -it registry.gitlab.inria.fr/starpu/starpu-docker/ci
```

- `registry.gitlab.inria.fr/starpu/starpu-docker/starpu:XXX` is a image
  containing a simple StarPU installation from the release XXX, with
  the MPI support enabled. The file recipe is
  [Dockerfile-starpu](./Dockerfile-starpu)

```bash
docker run -it registry.gitlab.inria.fr/starpu/starpu-docker/starpu:1.4.7
```

If the host machine has some GPUs, you can run

```bash
docker run -it --gpus all registry.gitlab.inria.fr/starpu/starpu-docker/starpu:1.4.7
```

StarPU will then see the GPUs devices of the host machine.

```bash
~$ starpu_machine_display -w CUDA -notopology
...
Real hostname: a71cc251527e (StarPU hostname: a71cc251527e)
3 CUDA workers:
	CUDA 0.0 (Quadro RTX 8000 40.0 GiB 81:00.0)
	CUDA 1.0 (Tesla T4 13.1 GiB 41:00.0)
	CUDA 2.0 (Tesla T4 13.1 GiB a1:00.0)
```

Images for the master version of StarPU and the latest stable releases
are installed. The list of tags is available at
https://gitlab.inria.fr/starpu/starpu-docker/container_registry/2057

If you need to mount your local directory

```bash
docker run -it --mount type=bind,src="$(pwd)",target=/home/starpu/mount registry.gitlab.inria.fr/starpu/starpu-docker/starpu:1.4.7
```

### Compose

A [compose.yaml](./compose.yaml) file is available to use with `docker
compose`. Starting a basic docker image to test StarPU is done as
follows.

- If you have a machine with GPU nodes, you can use

```bash
docker-compose run --rm igpu
```

- For a machine without GPU, you can use

```bash
docker-compose run --rm icpu
```
